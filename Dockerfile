FROM openjdk:11

COPY target/ /app/

EXPOSE 8761

WORKDIR /app/

ENTRYPOINT ["java", "-jar", "eureka-server.jar"]